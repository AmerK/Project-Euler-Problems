package nr9_special_pythagorean_triplet;

//A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
//
//        a2 + b2 = c2
//        For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
//
//        There exists exactly one Pythagorean triplet for which a + b + c = 1000.
//        Find the product abc.

public class SpecialTriplet {
    public static void main(String[] args) {

        int a = 0;
        int b = 0;
        int c = 0;
        int sum = 1000;

        for (int i = 10; i <= sum/3; i++)
        {
            if (a + b + c == 1000 && (powerItUp(a) + powerItUp(b) == powerItUp(c)))
            {
                break;
            }

            a = 0;
            b = 0;
            for (int j = 10; j <= sum/2; j++){
                    if (a + b + c == 1000 && (powerItUp(a) + powerItUp(b) == powerItUp(c)))
                    {
                        break;
                    }
                    a = i;
                    b = j;
                    c = sum - a - b;
                }
            }
        System.out.println("a = " + a + " b = " + b + " c = " + c);
        System.out.println("a + b + c = 1000 : " + a + " + " + b + " + " + c + " = 1000" );
        System.out.println("a^2 + b^2 = c^2 : " + powerItUp(a) + " + " + powerItUp(b) + " = " + powerItUp(c));
        }

    public static int powerItUp (int x)
    {
        return x*x;
    }

    }
