package nr5_SmallestMultiple;

//        2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//
//        What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

public class SmallestMultiple {
    public static void main(String[] args) {

        int myMultiple = 0;
        int temp;
        boolean done = false;

            for (int i = 20; i >= 20; i++)
            {
                if (done)
                {
                    break;
                }
                myMultiple = i;
                for (int j = 11; j <= 20; j++)
                {
                    if (myMultiple % j != 0)
                    {
                        break;
                    }
                    temp = myMultiple / j;
                    if (temp % 2 != 0)
                    {
                        break;
                    }
                    if (j == 20)
                    {
                        done = true;
                    }
                }
            }

        System.out.println("The smalles positive number that is evenly divisible by all of the number from 1 to 20 " +
                " is: " + myMultiple);
        }

    }

