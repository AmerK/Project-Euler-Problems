package nr8_largest_product_in_a_series;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.CharSink;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* The four adjacent digits in the 1000-digit number that have the greatest product are 9 × 9 × 8 × 9 = 5832.

73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450

Find the thirteen adjacent digits in the 1000-digit number that have the
 greatest product. What is the value of this product? */

public class LargestProduct {
    public static void main(String[] args) throws IOException {

        CharSink charSink = Files.asCharSink(new File("1000-digit_number.txt"), Charsets.UTF_8);
        charSink.write("73167176531330624919225119674426574742355349194934\n" +
                "96983520312774506326239578318016984801869478851843\n" +
                "85861560789112949495459501737958331952853208805511\n" +
                "12540698747158523863050715693290963295227443043557\n" +
                "66896648950445244523161731856403098711121722383113\n" +
                "62229893423380308135336276614282806444486645238749\n" +
                "30358907296290491560440772390713810515859307960866\n" +
                "70172427121883998797908792274921901699720888093776\n" +
                "65727333001053367881220235421809751254540594752243\n" +
                "52584907711670556013604839586446706324415722155397\n" +
                "53697817977846174064955149290862569321978468622482\n" +
                "83972241375657056057490261407972968652414535100474\n" +
                "82166370484403199890008895243450658541227588666881\n" +
                "16427171479924442928230863465674813919123162824586\n" +
                "17866458359124566529476545682848912883142607690042\n" +
                "24219022671055626321111109370544217506941658960408\n" +
                "07198403850962455444362981230987879927244284909188\n" +
                "84580156166097919133875499200524063689912560717606\n" +
                "05886116467109405077541002256983155200055935729725\n" +
                "71636269561882670428252483600823257530420752963450");

        File myFileWithBigNumber = new File("1000-digit_number.txt");
        List <String> myBigNumber = Files.readLines(myFileWithBigNumber, Charsets.UTF_8);

        List<String> splittedMyBigNumber = new ArrayList<>();

        String[] temporalContainer;

        for (int i = 0; i < myBigNumber.size(); i++)
        {
            temporalContainer = myBigNumber.get(i).split("");
            splittedMyBigNumber.addAll(Arrays.asList(temporalContainer));

//            for (String e : temporalContainer)
//            {
//                splittedMyBigNumber.add(e);
//            }
        }

        List<Integer> intOfSplittedMyBigNumber = new ArrayList<>();

        for (String element : splittedMyBigNumber)
        {
            intOfSplittedMyBigNumber.add(Integer.valueOf(element));
        }

        System.out.println(intOfSplittedMyBigNumber);

        int max = 0;
        int temp = 0;
        int[] numbersForHighestValue = new int[13];

        for (int i = 0; i < intOfSplittedMyBigNumber.size()-12; i++)
        {
            temp = intOfSplittedMyBigNumber.get(i);
//            temp = intOfSplittedMyBigNumber.get(i)*intOfSplittedMyBigNumber.get(i+1)*intOfSplittedMyBigNumber.get(i+2)
//                    *intOfSplittedMyBigNumber.get(i+3)*intOfSplittedMyBigNumber.get(i+4)*intOfSplittedMyBigNumber.get(i+5)*
//                    intOfSplittedMyBigNumber.get(i+6)*intOfSplittedMyBigNumber.get(i+7)*intOfSplittedMyBigNumber.get(i+8)*
//                    intOfSplittedMyBigNumber.get(i+9)* intOfSplittedMyBigNumber.get(i+10)*intOfSplittedMyBigNumber.get(i+11)*
//                    intOfSplittedMyBigNumber.get(i+12);
            for (int k = 1; k < 13; k++){
                temp *= intOfSplittedMyBigNumber.get(i+k);
            }
            if (temp > max)
            {
                max = temp;
                for (int j = 0; j < 13; j++)
                {
                    numbersForHighestValue[j] = intOfSplittedMyBigNumber.get(i+j);
                }
            }
        }

        System.out.print("Highest product out of 13 adjacent numbers is: " + max + " and it's made out of : ");
        for (int elements : numbersForHighestValue)
        {
            System.out.print(elements + " * ");
        }

    }
}
