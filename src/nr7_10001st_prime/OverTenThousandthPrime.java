package nr7_10001st_prime;

//By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
//
//        What is the 10 001st prime number?

public class OverTenThousandthPrime
{
    public static void main(String[] args)
    {
        int primeNumber = 2;
        int primeCalculate = 0;


        for (int i = 2; primeCalculate < 10001 ; i += 2)
        {
            for (int j = 2; j <= i; j += 2)
            {
                if (i % j == 0)
                {
                    if (j == i)
                    {
                        primeNumber = i;
                        primeCalculate++;
                    }
                    break;
                }

                while (j == 2)
                {
                    j++;
                }
            }
            while (i == 2)
            {
                i++;
            }
        }

        System.out.println("The 10 001st prime number is: " + primeNumber);
    }
}
