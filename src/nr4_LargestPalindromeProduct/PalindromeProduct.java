package nr4_LargestPalindromeProduct;

//        A palindromic number reads the same both ways.
//        The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
//
//        Find the largest palindrome made from the product of two 3-digit numbers.

public class PalindromeProduct {
    public static void main(String[] args) {

        int myNumber = 0;
        int myNumberFromBehind = 0;
        int palindromicNumber = 0;
        int temp;
        int i;
        int j = 0;
        boolean palindromeStartsWith0 = false;
        boolean isPalindromeFound = false;

        for (i = 999; i >= 100; i--){
            if (isPalindromeFound){
                break;
            }
            for (j = 999; j >= 100; j--){
                myNumber = i*j;
                temp = myNumber;
                while (temp > 0){

                    myNumberFromBehind = myNumberFromBehind * 10 + (temp % 10) ;

//                    if (myNumberFromBehind == 0){
//                        palindromeStartsWith0 = true;
//                        continue;
//                    }

                    temp = (temp - (temp % 10)) / 10;
                }
//                if (palindromeStartsWith0 == true) {
//                    continue;
//                } else
                    if (myNumber == myNumberFromBehind){

                    palindromicNumber = myNumber;
                    isPalindromeFound = true;
                    break;
                }

                myNumberFromBehind = 0;
//                palindromeStartsWith0 = false;
            }
        }

        System.out.println("The largest palindrome made from two 3-digit numbers is: "
                + i + " x " + j + " = " + palindromicNumber);
    }
}
