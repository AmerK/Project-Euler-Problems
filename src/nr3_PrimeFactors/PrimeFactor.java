package nr3_PrimeFactors;


//        The prime factors of 13195 are 5, 7, 13 and 29.
//
//        What is the largest prime factor of the number 600851475143 ?


import java.math.BigInteger;

public class PrimeFactor {
    public static void main(String[] args) {
        long primeFactor = 2;
        long bigNumber = 600851475143L;
        long licznikPrime = 0;

        for (long i = 2; i <= bigNumber; i += 2) {
            if (i == 2){
                i++;
            }
            if (bigNumber % i == 0)
            {
                bigNumber /= i;
            }
            primeFactor = i;

//            for (long j = 3; j < i/2; j += 2) {
//                if (i % j == 0) {
//                    licznikPrime = 1;
//                }
//            }
//            if (licznikPrime == 0){
//                primeFactor = i;
//                break;
//            }
//            licznikPrime = 0;
        }

        System.out.println("Largest prime factor of number 600851475143 is: " + primeFactor);
    }
}
