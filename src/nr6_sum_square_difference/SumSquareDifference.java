package nr6_sum_square_difference;


//The sum of the squares of the first ten natural numbers is,

//        12 + 22 + ... + 102 = 385
//        The square of the sum of the first ten natural numbers is,
//
//        (1 + 2 + ... + 10)2 = 552 = 3025
//        Hence the difference between the sum of the squares of the first ten natural numbers
//        and the square of the sum is 3025 − 385 = 2640.
//
//        Find the difference between the sum of the squares of the first one hundred natural numbers
//        and the square of the sum.

public class SumSquareDifference {
    public static void main(String[] args)
    {

        int sumOfSquares = 0;
        int squareOfSum = 0;

        for (int i = 1; i <= 100; i++ )
        {

            sumOfSquares += (i*i);

        }

        for (int i = 1; i <= 100; i++)
        {
            squareOfSum += i;
        }

        System.out.println("Sum of the squares of the first one hundred numbers: " + sumOfSquares);
        System.out.println("Sum of the first one hundred numbers: " + squareOfSum);
        System.out.print("Square of the sum of the first one hundred numbers: " + squareOfSum + "^2");
        squareOfSum *= squareOfSum;
        System.out.println(" = " + squareOfSum);
        System.out.println("Difference between these squares is: " + squareOfSum + " - " + sumOfSquares + " = "
        + (squareOfSum - sumOfSquares));
    }
}
