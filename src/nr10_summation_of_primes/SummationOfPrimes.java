package nr10_summation_of_primes;

//        The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//
//        Find the sum of all the primes below two million.

public class SummationOfPrimes {
    public static void main(String[] args) {

        long sumOfPrimes = 2;
        boolean isPrime = true;

        for (int i = 3; i < 2_000_000; i += 2)
        {
            for (int j = 2; j*j <= i; j += 2)
            {
                while (j == 2)
                {
                    if (i % j == 0)
                    {
                        isPrime = false;
                        break;
                    }
                    j++;
                }
                if (i % j == 0)
                {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime)
            {
                sumOfPrimes += i;
            }
            isPrime = true;
        }

        System.out.println("The summation of primes below 2 million is equal to: " + sumOfPrimes);
    }
}
